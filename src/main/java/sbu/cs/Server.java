package sbu.cs;

import java.io.IOException;

public class Server {
    public static void main(String[] args) throws IOException {
        String directory = args[0];
//        String fileName = args[1];
        MyServer server = new MyServer();
        FileManager fileManager = new FileManager();
        fileManager.receiveAndSaveFile(directory, server);
        server.closeConnection();
    }
}
