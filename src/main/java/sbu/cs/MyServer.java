package sbu.cs;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class MyServer {
    private static final int portNumber = 4444;

    public Socket socket = null;
    public DataInputStream input = null;
    public ServerSocket serverSocket = null;
    public FileOutputStream fileOutput = null;
    public BufferedOutputStream bufferedOutput = null;


    public MyServer() {
        try {
            serverSocket = new ServerSocket(portNumber);
            System.out.println("Waiting for client...");
            socket = serverSocket.accept();
            input = new DataInputStream(socket.getInputStream());

        } catch (Exception e) {
            System.out.println(e);
        }

    }

    public void closeConnection(){
        try {
            input.close();
            socket.close();
            input.close();
            socket.close();
            fileOutput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
