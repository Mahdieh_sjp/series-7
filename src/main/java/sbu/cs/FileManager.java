package sbu.cs;

import java.io.*;


public class FileManager {

    public final static int FILE_SIZE = 40022386;

    public void sendFile(String filePath, MyClient client) throws IOException {
        File file = new File(filePath);
        String fileName = file.getName();

        byte [] byteArray  = new byte [(int)file.length()];
        client.input = new FileInputStream(file);
        client.bufferedInput = new BufferedInputStream(client.input);
        client.bufferedInput.read(byteArray, 0, byteArray.length);
        client.out.writeUTF(fileName);
        System.out.println("Sending " + filePath + "(" + byteArray.length + " bytes)");
        client.out.write(byteArray,0, byteArray.length);
        client.out.flush();
        System.out.println("Done.");

    }

    public void receiveAndSaveFile(String directory, MyServer server) throws IOException {
        byte [] byteArray  = new byte [FILE_SIZE];
        int count, current;
        String fileName = server.input.readUTF();
        server.fileOutput = new FileOutputStream(directory + "/" + fileName);
        server.bufferedOutput = new BufferedOutputStream(server.fileOutput);
        count = server.input.read(byteArray,0, byteArray.length);
        current = count;

        do {
            count =
                    server.input.read(byteArray, current, (byteArray.length-current));
            if(count >= 0) current += count;
        } while(count > -1);

        server.bufferedOutput.write(byteArray, 0 , current);
        server.bufferedOutput.flush();
        System.out.println("File " + directory
                + " downloaded (" + current + " bytes)");
    }

}
