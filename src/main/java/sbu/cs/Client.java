package sbu.cs;

import java.io.IOException;

public class Client {
    public static void main(String[] args) {
        String filePath = args[0];

        MyClient client = new MyClient("localhost", 4444);
        FileManager fileManager = new FileManager();
        try {
            fileManager.sendFile(filePath, client);
        } catch (IOException e) {
            e.printStackTrace();
        }

        client.closeConnection();
    }
}

