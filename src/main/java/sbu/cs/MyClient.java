package sbu.cs;

import java.io.*;
import java.net.Socket;

public class MyClient {
    public Socket socket = null;
    public InputStream input = null;
    public DataOutputStream out = null;
    public BufferedInputStream bufferedInput = null;


    public MyClient(String address, int port) {
        try {
            socket = new Socket(address, port);
            System.out.println("Connected to server");
            out = new DataOutputStream(socket.getOutputStream());

        } catch (Exception e) {
            System.out.println(e);
        }
    }

    public void closeConnection(){
        try {
            out.close();
            socket.close();
            bufferedInput.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
